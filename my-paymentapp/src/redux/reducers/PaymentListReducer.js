import {PAYMENT_LIST_REQUEST, PAYMENT_LIST_SUCCESS, PAYMENT_LIST_FAIL}
    from "../constants/PaymentConstants";

const paymentsInitialState = {
    pending: false,
    payments: [],
    error: null
}
//if state is defined use state otherwise use paymentsInitialState
function PaymentListReducer(state=paymentsInitialState,action)
{
    switch (action.type) {
        case PAYMENT_LIST_REQUEST:
            return { ...state, pending: true };
        case PAYMENT_LIST_SUCCESS:
            return { ...state, pending: false, payments: action.payload };
        case PAYMENT_LIST_FAIL:
            return { ...state, pending: false, error: action.payload };
        default:
            return state;
    }
}
export default PaymentListReducer;
