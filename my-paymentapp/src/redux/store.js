import {applyMiddleware, createStore} from "redux";
import  PaymentListReducer from "./reducers/PaymentListReducer"
import thunk from "redux-thunk";
import {createLogger} from "redux-logger";

var logger = createLogger({
    collapsed: true
});


const initialState = {};
export const middlewares = [thunk, logger];
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ //|| compose;

export const store = createStore(
    PaymentListReducer, //typically combine all reducers
    initialState,
    //  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    composeEnhancer(applyMiddleware(...middlewares))
);
//export default store;