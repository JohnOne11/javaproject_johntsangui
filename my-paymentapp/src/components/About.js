import http from "../http-common";
import React, {Component} from 'react';

//extends Component gives access to lifecycle methods
export default class About extends Component
{
    constructor(props) {
        super(props);
        this.state = {name: "Allstate", year: "2020", apistatus: "Unknown"}
    }

    render() {
        return ( <div>
                <h3>Status is {this.state.apistatus}</h3>
            </div>
        )
    }

    async componentDidMount() {
        let response
        try {
            response = await http.get("/status");
        } catch (e) {
            console.log('Error encountered: ' + e.toString())
        }
        this.setState({apistatus: response.data});
    }
}