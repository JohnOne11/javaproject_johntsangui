import React, {Component} from 'react';
import PaymentService from "../services/PaymentRestService";
import {Redirect} from 'react-router-dom';

export default class AddEmployees extends Component {
    constructor(props) {
        super(props);
        this.onChangeId = this.onChangeId.bind(this);
        this.onChangeType = this.onChangeType.bind(this);
        this.onChangePaymentDate = this.onChangePaymentDate.bind(this);
        this.onChangeCustId = this.onChangeCustId.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.savePayment = this.savePayment.bind(this);

        this.state = {
            id: 0,
            type: "",
            paymentdate: "",
            custid: 0,
            amount: 0,
            submitted: false
        };
    }
    onChangeId(e) {
        this.setState({
            id: e.target.value
        });
    }
    onChangeType(e) {
        this.setState({
            type: e.target.value
        });
    }
    onChangePaymentDate(e) {
        this.setState({
            paymentdate: e.target.value
        });
    }
    onChangeCustId(e) {
        this.setState({
            custid: e.target.value
        });
    }
    onChangeAmount(e) {
        this.setState({
            amount: e.target.value
        });
    }

    savePayment() {
        var data = {
            id: this.state.id,
            type: this.state.type,
            paymentdate: this.state.paymentdate,
            custid: this.state.custid,
            amount: this.state.amount        }

        PaymentService.create(data)
            .then(response => {
                this.setState({
                    submitted: true
                });

            })
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }

    render() {
        if (this.state.submitted) {
            return <Redirect to="home"/>
        }

        return (<form>
                <h1>Add Payment</h1>
                <div className="form-group">
                    <label htmlFor="exampleId">ID number</label>
                    <input type="number" className="form-control" id="exampleId"
                           placeholder="Enter ID number" value={this.state.id} onChange={this.onChangeId}/>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleType">Account Type</label>
                    <input type="text" className="form-control" id="exampleType"
                           placeholder="Enter Account Type" value={this.state.type} onChange={this.onChangeType}/>
                </div>
                <div className="form-group">
                    <label htmlFor="examplePaymentDate">Payment Date</label>
                    <input type="date" className="form-control" id="examplePaymentDate"
                           placeholder="Enter Payment Date" value={this.state.paymentdate} onChange={this.onChangePaymentDate}/>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleAmount">Payment Amount</label>
                    <input type="number" className="form-control" id="exampleAmount"
                           placeholder="Enter Amount" value={this.state.amount} onChange={this.onChangeAmount}/>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleCustid">Customer Id</label>
                    <input type="number" className="form-control" id="exampleCustid"
                           placeholder="Enter Customer id" value={this.state.custid} onChange={this.onChangeCustId}/>
                </div>
                <div className="form-group">
                    <input type="button" className="form-control" value="Save" onClick={this.savePayment}/>
                </div>

            </form>
        )
    }
}