import React, {Component} from 'react';
import paymentList from "../redux/actions/PaymentListActions";
import connect from "react-redux/lib/connect/connect";
import './ListPaymentsRedux.css';

//extends Component gives access to lifecycle methods
class ListPaymentsRedux extends Component {
    constructor(props) {
        super(props)
        this.props.dispatch(paymentList())
    }

    render(){
        //destructuring
        const {payments} = this.props;
        return  (
            <div id="headdiv" className="col-md-6">
                <h4 id="heading">Payments List</h4>
                <ul className="list-group">
                    {payments &&
                    payments.map((payment, index) => (
                        <li className={"list-group-item "}
                                 // + (index === 0 ? "active" : "")}
                            onClick={() => toggleDetails('paymentsLabel', index)}
                            key={index}>
                                {/*Refactor with accordion*/}
                                Customer ID: {payment.custid}
                                <label id={'paymentsLabel'+(index)}>
                                    <p>
                                        <span><b>Type: </b> {payment.type}</span>
                                        <span><b>Date: </b> {payment.paymentdate}</span>
                                        <span><b>Amount: </b> {payment.amount}</span>
                                    </p>
                                </label>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: () => dispatch(paymentList())
    };
};

const mapStateToProps = state => ({
    error:state.error,
    payments: state.payments,
    pending: state.pending
});

function toggleDetails(labelind, index) {
    let x = document.getElementById(labelind+index);

    if (x.style.display ==="none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(ListPaymentsRedux);