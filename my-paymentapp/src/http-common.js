import axios from "axios";

//.create so all classes that import http-common will have baseURL and headers set
export default axios.create({
    // api is from project endtoend in StatusController
    baseURL: "http://localhost:8080/api",
    headers: {
        "Content-type": "application/json"
    }
});