import http from "../http-common";
export class PaymentService {
    getAll() {
        // all comes from PaymentController
        return http.get("/all");
    }
    create(data) {
        return http.post("/save", data);
    }
}
//when this class is imported, it will be instantiated
export default new PaymentService();